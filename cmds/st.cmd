require asyn,4.37.0
require xtpico,0.9.1
require bpmrffeioc,0.9.0

errlogInit(20000)

#- load the instance definition
#< instance.cmd
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "XP-006")
epicsEnvSet("DEVICE_IP", "172.30.150.59")


#- 1 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "1000000")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
#- asyn IP comm ports
epicsEnvSet("I2C_COMM_PORT",                "I2C_COMM")
epicsEnvSet("SPI_COMM_PORT",                "SPI_COMM")

#- Create the asyn port to talk XTpico PORT1; TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

#- Create the asyn port to talk XTpico PORT2; TCP port 1003.
drvAsynIPPortConfigure($(SPI_COMM_PORT),"$(DEVICE_IP):1003")
#- asynSetTraceIOMask("$(SPI_COMM_PORT)",0,255)
#- asynSetTraceMask("$(SPI_COMM_PORT)",0,255)

#- one VCXO/PLL
iocshLoad("$(bpmrffeioc_DIR)/lmx2582.iocsh", "IP_PORT=$(SPI_COMM_PORT), N=1, NAME=PLL1")
#- one digital attenuator
iocshLoad("$(bpmrffeioc_DIR)/hmc624a.iocsh", "IP_PORT=$(SPI_COMM_PORT), N=1, NAME=ATT1")
#- one port expander
iocshLoad("$(bpmrffeioc_DIR)/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=IO1, COUNT=1, INFOS=0x25")
#- three temperature readouts
iocshLoad("$(bpmrffeioc_DIR)/adt7420.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=TMP1, COUNT=1, INFOS=0x48 0x73 3")
iocshLoad("$(bpmrffeioc_DIR)/adt7420.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=2, NAME=TMP2, COUNT=1, INFOS=0x49 0x73 3")
iocshLoad("$(bpmrffeioc_DIR)/adt7420.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=3, NAME=TMP3, COUNT=1, INFOS=0x4B 0x73 3")
#- one serial number
iocshLoad("$(bpmrffeioc_DIR)/ds28cm00.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=SN1, COUNT=1, INFOS=0x50 0x73 6")
#- one voltage monitor
iocshLoad("$(bpmrffeioc_DIR)/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1, NAME=VM1, COUNT=1, INFOS=0x48 0x73 0")

#- high level front end controls
iocshLoad("$(bpmrffeioc_DIR)/rffe.iocsh", "IO=IO1,ATT=ATT1,PLL=PLL1")

# set_requestfile_path("$(DB_DIR)")
# set_requestfile_path("./")
# set_savefile_path("$(AUTOSAVE_DIR)")

# #- apply default PV values (located in IOC_DIR)
# set_pass0_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")
# set_pass1_restoreFile("$(IOC_DIR)/default_settings.sav", "P=$(PREFIX),R=")
# #- apply runtime changed PV values (located in AUTOSAVE_DIR)
# set_pass0_restoreFile("info_positions.sav")
# set_pass1_restoreFile("info_settings.sav")

# save_restoreSet_status_prefix("$(PREFIX)")
# dbLoadRecords("save_restoreStatus.db","P=$(PREFIX)")

###############################################################################
iocInit()
###############################################################################

#- build info_positions.req file from record info 'autosaveFields_pass0' field
#- build info_settings.req file from record info 'autosaveFields' field
# makeAutosaveFiles("")
# create_monitor_set("info_positions.req", 5)
# #- save things every thirty seconds
# create_monitor_set("info_settings.req", 30)

# date
###############################################################################
