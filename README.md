# BPM RF Front-end IOC

This module contains the EPICS databases and IOC snippets for the BPM RFFE module. The BPM RFFE are the electronics responsible for the RF pre-processing of the BPM antenna signals, prior to the digitalization by the MTCA systems.
These electronics include a XT-PICO module that allows remote control and monitor of certain parameters. This EPICS module implements the communication with the XT-PICO and the particular settings for the BPM systems.

---

### Prerequisites

All files of this module derives from the [xtpico](https://gitlab.esss.lu.se/e3/wrappers/devices/e3-xtpico.git) module, so it is mandatory to have the **xtpico** module loaded when running any IOC that makes use of the **bpmrffeioc** module. **xtpico** is also define as a dependency of this project.

### Upstream module

This repository is the E3 wrapper of the [bpmrffeioc](https://gitlab.esss.lu.se/beam-diagnostics/bde/iocs/bpmrffeioc.git) module, maintained by Beam Instrumentation Group.
